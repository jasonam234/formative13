SELECT p.name as "Product Name", b.name as "Brand Name", m.name as "Manufacturer Name", a.street as "Manufacture Address",pri.amount as "Price", v.name as "Currency Name", p.stock FROM product as p
JOIN brand as b ON b.id = p.brandId
JOIN manufacturer as m ON m.id = p.manufactureId
JOIN price as pri ON pri.productId = p.id
JOIN valuta as v ON v.id = pri.valutaId
JOIN address as a ON a.id = m.addressId
WHERE p.stock = 0
ORDER BY p.name;