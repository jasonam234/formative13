SHOW databases;
create database formativedb;
use formativedb;
show databases;

CREATE TABLE address(
	id integer NOT NULL AUTO_INCREMENT UNIQUE,
    street varchar(50),
    city varchar(20),
    province varchar(20),
    country varchar(20),
    zipCode integer,
    PRIMARY KEY(id)
);

CREATE TABLE manufacturer(
	id integer NOT NULL AUTO_INCREMENT UNIQUE,
    name varchar(20),
    addressId integer NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(addressId) REFERENCES address(id)
);

CREATE TABLE brand(
	id integer NOT NULL AUTO_INCREMENT UNIQUE,
    name varchar(20),
    PRIMARY KEY(id)
);

CREATE TABLE product(
	id integer NOT NULL AUTO_INCREMENT UNIQUE,
    artNumber integer UNIQUE,
    name varchar(30),
    description varchar(50),
    manufactureId integer NOT NULL,
    brandId integer NOT NULL,
    stock integer,
    PRIMARY KEY(id),
    FOREIGN KEY(brandId) REFERENCES brand(id),
    FOREIGN KEY(manufactureId) REFERENCES manufacturer(id)
);

CREATE TABLE valuta(
	id integer NOT NULL AUTO_INCREMENT UNIQUE,
    code varchar(5),
    name varchar(20),
    PRIMARY KEY(id)
);

CREATE TABLE price(
	id integer NOT NULL AUTO_INCREMENT UNIQUE,
    productId integer NOT NULL,
    valutaId integer NOT NULL,
    amount integer,
    PRIMARY KEY(id),
    FOREIGN KEY(productId) REFERENCES product(id),
    FOREIGN KEY(valutaId) REFERENCES valuta(id)
);