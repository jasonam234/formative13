SELECT pro.name as "Product Name", m.name as "Manufacturer Name", a.street as "Manufacturer Address", b.name as "Brand Name", v.code as "Currency", p.amount as "Price" FROM price as p 
JOIN product as pro ON pro.id = p.productId
JOIN manufacturer as m ON m.id = pro.manufactureId
JOIN address as a ON a.id = m.addressId
JOIN brand as b ON b.id = pro.brandId
JOIN valuta as v ON v.id = p.valutaId
ORDER BY p.amount;