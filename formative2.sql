INSERT INTO address(street, city, province, country, zipCode) VALUES("Jalan A No. 11 A", "Jakarta", "DKI Jakarta", "Indonesia", "11140");
INSERT INTO address(street, city, province, country, zipCode) VALUES("Jalan B No. 22 B", "Bandung", "Jawa Barat", "Indonesia", "40111");
SELECT * FROM address;

INSERT INTO manufacturer(name, addressId) VALUES("Microsoft Indonesia", "1");
INSERT INTO manufacturer(name, addressId) VALUES("Apple Indonesia", "2");
SELECT * FROM manufacturer;

INSERT INTO brand(name) VALUES("Microsoft");
INSERT INTO brand(name) VALUES("Apple");
SELECT * FROM brand;

INSERT INTO product(artNumber, name, description, manufactureId, brandId, stock) VALUES("1", "Iphone 5", "Cheapest iphone on the market", "2", "2", "100");
INSERT INTO product(artNumber, name, description, manufactureId, brandId, stock) VALUES("2", "Iphone 6", "Most used iphone", "2", "2", "50");
INSERT INTO product(artNumber, name, description, manufactureId, brandId, stock) VALUES("3", "Iphone 7", "Newest iphone on the market", "2", "2", "70");
INSERT INTO product(artNumber, name, description, manufactureId, brandId, stock) VALUES("4", "Iphone 8", "newest flagship iphone", "2", "2", "20");
INSERT INTO product(artNumber, name, description, manufactureId, brandId, stock) VALUES("5", "Iphone 9", "Hardest iphone to get", "2", "2", "0");
INSERT INTO product(artNumber, name, description, manufactureId, brandId, stock) VALUES("6", "Windows XP", "Oldest OS on the shop", "1", "1", "0");
INSERT INTO product(artNumber, name, description, manufactureId, brandId, stock) VALUES("7", "Windows Vista", "Hardest OS to get", "1", "1", "1");
INSERT INTO product(artNumber, name, description, manufactureId, brandId, stock) VALUES("8", "Windows 7", "Most used OS", "1", "1", "1000");
INSERT INTO product(artNumber, name, description, manufactureId, brandId, stock) VALUES("9", "Windows 10", "Most intuitive Windows OS", "1", "1", "100");
INSERT INTO product(artNumber, name, description, manufactureId, brandId, stock) VALUES("10", "Windows 11", "Newest Windows OS on the market", "1", "1", "70");
SELECT * FROM product;

INSERT INTO valuta(code, name) VALUES("IDR", "Rupiah");
INSERT INTO valuta(code, name) VALUES("USD", "US Dollar");
SELECT * FROM valuta;

INSERT INTO price(productId, valutaId, amount) VALUES(1, 1, 3500000);
INSERT INTO price(productId, valutaId, amount) VALUES(1, 2, 350);
INSERT INTO price(productId, valutaId, amount) VALUES(2, 1, 4500000);
INSERT INTO price(productId, valutaId, amount) VALUES(2, 2, 450);
INSERT INTO price(productId, valutaId, amount) VALUES(3, 1, 7000000);
INSERT INTO price(productId, valutaId, amount) VALUES(3, 2, 700);
INSERT INTO price(productId, valutaId, amount) VALUES(4, 1, 8000000);
INSERT INTO price(productId, valutaId, amount) VALUES(4, 2, 800);
INSERT INTO price(productId, valutaId, amount) VALUES(5, 1, 12000000);
INSERT INTO price(productId, valutaId, amount) VALUES(5, 2, 1200);
INSERT INTO price(productId, valutaId, amount) VALUES(6, 1, 30000);
INSERT INTO price(productId, valutaId, amount) VALUES(6, 2, 3);
INSERT INTO price(productId, valutaId, amount) VALUES(7, 1, 10000);
INSERT INTO price(productId, valutaId, amount) VALUES(7, 2, 1);
INSERT INTO price(productId, valutaId, amount) VALUES(8, 1, 120000);
INSERT INTO price(productId, valutaId, amount) VALUES(8, 2, 12);
INSERT INTO price(productId, valutaId, amount) VALUES(9, 1, 200000);
INSERT INTO price(productId, valutaId, amount) VALUES(9, 2, 20);
INSERT INTO price(productId, valutaId, amount) VALUES(10, 1, 350000);
INSERT INTO price(productId, valutaId, amount) VALUES(10, 2, 35);
SELECT * FROM price;

